﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.PugPigConnector.BusinessLogic.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Umbraco.PugPigConnector.BusinessLogic.Controllers
{
    public class PugPigEditionController : RenderMvcController
    {
        public ActionResult PugEdition(RenderModel rm)
        {

            var model = new Edition(rm.Content);

            
            //return View("~/App_Plugins/PugPigConnector/PugEdition.cshtml", model);

            return CurrentTemplate(model);
        }
    }

}
