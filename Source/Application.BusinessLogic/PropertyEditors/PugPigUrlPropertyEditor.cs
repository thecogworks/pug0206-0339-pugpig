﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientDependency.Core;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web.PropertyEditors;

namespace Umbraco.PugPigConnector.BusinessLogic.PropertyEditors
{
    [PropertyEditor("PugPig.UrlLabel", "PugPig Url Label", "/App_Plugins/PugPigConnector/UrlPropertyEditor/urllabel.html", ValueType = "TEXT")]
    [PropertyEditorAsset(ClientDependencyType.Javascript, "/App_Plugins/PugPigConnector/UrlPropertyEditor/urllabel.controller.js")]
    public class PugPigUrlPropertyEditor : PropertyEditor
    {
        

    }
}
