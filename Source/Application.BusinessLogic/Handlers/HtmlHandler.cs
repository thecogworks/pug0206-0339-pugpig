﻿using System.Web;
using Umbraco.PugPigConnector.BusinessLogic.Helpers;

namespace Umbraco.PugPigConnector.BusinessLogic.Handlers
{
    public class HtmlHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext httpContext)
        {
            // Get the equivalent Umbraco URL from the request - in this case just strip the file extension
            var originalUrl = httpContext.Request.Url.PathAndQuery;
            var umbracoPath = originalUrl.Replace(".html", string.Empty);

            // Search for a matching node by path (or ID this is passed in the original request)
            var node = HttpHandlerHelper.GetUmbracoNodeByPath(httpContext, umbracoPath);

            if (node != null)
            {
                // Return the content of the Umbraco route
                httpContext.Server.TransferRequest(node.Url);
            }
            else
            {
                HttpHandlerHelper.ProcessRequestWithHandler(httpContext, "System.Web.StaticFileHandler");   
            }

        }
    }
}
