﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.PugPigConnector.BusinessLogic.Extensions;
using Umbraco.Web;

namespace Umbraco.PugPigConnector.BusinessLogic.Models
{
    public class Publication : PublishedContentModel
    {
        public Publication(IPublishedContent content) : base(content) {}

        public string Title
        {
            get { return this.GetPropertyValue<string>("title"); }
        }

        public string Updated
        {
            get { return this.UpdateDate.ToPugDate(); }
        }

        public string PugPigUrl
        {
            get { return this.GetPropertyValue<string>("pugPigUrl"); }
        }


        private IEnumerable<Edition> _editions;
        public IEnumerable<Edition> Editions
        {
            get
            {
                if (_editions == null)
                {
                    var childNodes = this.Content.Children.ToList().Where(x => x.DocumentTypeAlias == "PugPigEdition");
                    _editions = childNodes.Select(item => new Edition(item));
                }
                return _editions;
            }
        }
    }
}