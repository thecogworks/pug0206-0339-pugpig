﻿
using System.Web.Mvc;
using Umbraco.PugPigConnector.BusinessLogic.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Umbraco.PugPigConnector.BusinessLogic.Controllers
{
    public class PugPigPublicationController : RenderMvcController
    {
        public ActionResult Index(RenderModel rm)
        {          
            var model = new Publication(rm.Content);

            return CurrentTemplate(model);
        }
    }

}
