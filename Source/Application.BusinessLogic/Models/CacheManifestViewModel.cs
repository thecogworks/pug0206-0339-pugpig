﻿namespace Umbraco.PugPigConnector.BusinessLogic.Models
{
    public class CacheManifestViewModel
    {
        public string Name { get; set; }
        public string UpdateDate { get; set; }
        public string PublicationAssets { get; set; }
        public string PageAssets { get; set; }
    }
}