﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using HtmlAgilityPack;
using Umbraco.Core.Logging;
using Umbraco.PugPigConnector.BusinessLogic.Helpers;
using Umbraco.PugPigConnector.BusinessLogic.Models;
using Umbraco.Web;

namespace Umbraco.PugPigConnector.BusinessLogic.Handlers
{
    public class CacheManifestHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext httpContext)
        {
            // Get the equivalent Umbraco URL from the request
            var originalUrl = httpContext.Request.Url;
            var originalLocalPath = originalUrl.PathAndQuery;

            var pageUrl = originalUrl.ToString().Replace(".manifest", ".html");
            var umbracoPath = originalLocalPath.Replace(".manifest", string.Empty);

            // Search for a matching node by path (or ID this is passed in the original request)
            var node = HttpHandlerHelper.GetUmbracoNodeByPath(httpContext, umbracoPath);

            if (node != null)
            {
                // Return the content of the Umbraco route
                httpContext.Server.TransferRequest(node.Url + "?manifest=true");
            }
            else
            {
                HttpHandlerHelper.ProcessRequestWithHandler(httpContext, "System.Web.StaticFileHandler");
            }

            //if (node != null && node.DocumentTypeAlias == "PugPigPage")
            //{
            //    try
            //    {
            
            //    }
            //    catch (Exception ex)
            //    {
            //        LogHelper.Error<CacheManifestHandler>("Error getting cache manifest", ex);

            //        // TODO: Better to handle this with http or Umbraco handler even..
            //        HttpHandlerHelper.ProcessRequestWithHandler(httpContext, "System.Web.StaticFileHandler"); 
            //    }
            //}
            //else
            //{
            //    HttpHandlerHelper.ProcessRequestWithHandler(httpContext, "System.Web.StaticFileHandler"); 
            //}

        }

        

        
    }
}
