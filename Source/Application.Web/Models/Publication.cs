﻿using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.PugPigConnector.BusinessLogic.Extensions;
using Umbraco.Web;

namespace Umbraco.PugPigConnector.Web.Models
{
    public class Publication : PublishedContentModel
    {
        public Publication(IPublishedContent content) : base(content) {}

        public string Title
        {
            get { return this.GetPropertyValue<string>("title"); }
        }

        public string Updated
        {
            get { return this.UpdateDate.ToPugDate(); }
        }

        public string FeedUrl
        {
            get { return this.Url.RemoveTrailingSlash(); }
        }

        public IEnumerable<Edition> Editions { get; set; }      
    }
}