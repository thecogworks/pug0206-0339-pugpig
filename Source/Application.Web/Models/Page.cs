﻿using System;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.PugPigConnector.BusinessLogic.Extensions;
using Umbraco.Web;

namespace Umbraco.PugPigConnector.Web.Models
{
    public class Page : PublishedContentModel
    {
        public Page(IPublishedContent content) : base(content) { }

        public string Title
        {
            get { return this.GetPropertyValue<string>("title") ?? this.Name;  }
        }

        public string Updated
        {
            get { return this.UpdateDate.ToPugDate(); }
        }

        public string Summary
        {
            get { return this.GetPropertyValue<string>("summary"); }
        }

        public string CacheManifestUrl
        {
            get { return this.GetPropertyValue<string>("cacheManifestUrl"); }
        }

        public string PermaLink
        {
            get { return string.Format("/{0}.aspx", this.Id); }
        }
    }
}