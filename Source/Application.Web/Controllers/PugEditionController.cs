﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.PugPigConnector.Web.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Umbraco.PugPigConnector.Web.Controllers
{
    public class PugEditionController : RenderMvcController
    {
        public ActionResult PugEdition(RenderModel rm)
        {

            var model = new Edition(rm.Content);

            var childNodes = rm.Content.Children.Where(x => x.DocumentTypeAlias == "PugPage");
            model.Pages = childNodes.Select(item => new Page(item));

            //return View("~/App_Plugins/PugPigConnector/PugEdition.cshtml", model);

            return CurrentTemplate(model);
        }
    }

}
