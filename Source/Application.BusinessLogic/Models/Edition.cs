﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.PugPigConnector.BusinessLogic.Extensions;
using Umbraco.Web;

namespace Umbraco.PugPigConnector.BusinessLogic.Models
{
    public class Edition : PublishedContentModel
    {

        public Edition(IPublishedContent content)
            : base(content)
        { }

        private DateTime _publishedDate
        {
            get
            {
                var date = this.GetPropertyValue<DateTime>("publishedDate") != DateTime.MinValue
                                                                            ? this.GetPropertyValue<DateTime>("publishedDate")
                                                                            : this.UpdateDate;
                return date;
            }
        }


        public string Title
        {
            get { return this.GetPropertyValue<string>("title"); }
        }

        public string Updated
        {
            get { return this.UpdateDate.ToPugDate(); }
        }

        public string Published
        {
            get { return _publishedDate.ToPugDate(); }
        }

        public string Issued
        {
            get
            {
                return this.GetPropertyValue<string>("publishedDate");
                //_publishedDate.ToString("yyyy-MM-dd");
            }
        }

        public string Author
        {
            get { return this.GetPropertyValue<string>("authorName"); }
        }

        public string Summary
        {
            get { return this.GetPropertyValue<string>("summary"); }
        }

        public bool IsFreeEdition
        {
            get { return this.GetPropertyValue<bool>("freeEdition"); }
        }

        public string PugPigUrl
        {
            get { return this.GetPropertyValue<string>("pugPigUrl"); }
        }

        public string CoverImage
        {
            get
            {
                var imageId = this.GetProperty("coverImage").DataValue.ToString();

                if (!string.IsNullOrEmpty(imageId))
                {
                    return imageId;
                    //return string.Format("..{0}", images.First().Url);
                }

                return string.Empty;
            }
        }

        public string FeedRel
        {
            get { return this.IsFreeEdition ? "http://opds-spec.org/acquisition" : "http://opds-spec.org/acquisition/buy"; }
        }

        public string Status
        {
            get { return this.GetPropertyValue<string>("pugStatus"); }
        }


        private IEnumerable<Page> _pages;
        public IEnumerable<Page> Pages
        {
            get
            {
                if (_pages == null)
                {
                    var childNodes = Content.Children.ToList().Where(x => x.DocumentTypeAlias == "PugPigPage");
                    _pages = childNodes.Select(item => new Page(item));
                }
                return _pages;
            }
        }


        public string IsDraftXml()
        {
            return this.Status == "Draft" ? "<app:control><app:draft>yes</app:draft></app:control>" : string.Empty;
        }
    }
}