# Pugpig Umbraco connector #

Starterkit package with which you can create pug pig publications. 

Install instructions

Install the package.  Move the newly create publications node to the root of your website.  Edit the content so that empty / missing images in edition and page nodes have images (NB creating a package does not allow addition of images from media section to the package hence missing images).  Publish publication and child pages.  In the properties tab of publication, editions and pages you will see pugpig urls. When using pugpig client reader you can set the ODPS url to publication url.


### How do I get set up? ###

* Install the package
* Move the publications node to root of your site
* Update content of editions and pages including any images
* Publish pages
* Open xml link in pugpig reader