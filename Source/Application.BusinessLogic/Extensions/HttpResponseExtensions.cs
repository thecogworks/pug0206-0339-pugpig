﻿using System;
using System.Web;

namespace Umbraco.PugPigConnector.BusinessLogic.Extensions
{
    public static class HttpResponseExtensions
    {
        public static void WriteLine(this HttpResponse httpResponse, string s)
        {
            httpResponse.Write(s);
            httpResponse.Write(Environment.NewLine);
        }


    }
}
