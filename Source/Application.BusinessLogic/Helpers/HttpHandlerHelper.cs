﻿using System;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Security;

namespace Umbraco.PugPigConnector.BusinessLogic.Helpers
{
    public static class HttpHandlerHelper
    {
        /// <summary>
        /// Gets last chunk of URL path (which should be a path of IDs. eg /1087/1523/1523), then returns the node.
        /// 
        /// Ensures that the paths of the ancestors are correct.
        /// </summary>
        /// <returns></returns>
        internal static IPublishedContent GetUmbracoNodeByPath(HttpContext httpContext, string umbracoPath)
        {
            // We need to ensure we have an Umbraco context so we can search for our node, see
            // http://issues.umbraco.org/issue/U4-5445#comment=67-16816
            var httpContextWrapper = new HttpContextWrapper(httpContext);
            UmbracoContext.EnsureContext(httpContextWrapper,
                                        ApplicationContext.Current,
                                        new WebSecurity(httpContextWrapper, ApplicationContext.Current),
                                        true);

            // get parts
            var urlChunks = umbracoPath.Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

            if (urlChunks.Count == 0) { return null; }

            // use chunk to get node
            int lastId;
            var node = int.TryParse(urlChunks.Last(), out lastId) ? UmbracoContext.Current.ContentCache.GetById(lastId) : null;

            // not found
            if (node == null) { return null; }

            // compare url chunks with node path to ensure the other parts of the path are correct
            var nodePathChunks = node.Path.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

            if (nodePathChunks.Count < 3){ return null; }

            nodePathChunks.RemoveRange(0, 2);
            var nodePathWithoutRoot = string.Join(",", nodePathChunks);

            if (nodePathWithoutRoot != string.Join(",", urlChunks)) { return null; }

            return node;
        }




        internal static void ProcessRequestWithHandler(HttpContext httpContext, string handlerType)
        {
            var type = typeof(HttpApplication).Assembly.GetType(handlerType, true);
            var handler = (IHttpHandler)Activator.CreateInstance(type, true);

            handler.ProcessRequest(httpContext);
        }


        
    }
}
