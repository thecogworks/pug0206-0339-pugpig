﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.PugPigConnector.Web.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Umbraco.PugPigConnector.Web.Controllers
{
    public class PugFeedController : RenderMvcController
    {
        public override ActionResult Feed(RenderModel rm)
        {
            var feedUrl = rm.Content.Url;
            var feedUrlAbsolute = rm.Content.UrlAbsolute();
            
            int editionId;
            if (int.TryParse(Request.QueryString["editionId"], out editionId)) // Return the edition feed
            {   
                var editionNode = Umbraco.TypedContent(editionId);
                // TODO: Try Catch exceptions (editionNode null)
                var model = new Edition(editionNode, feedUrl);

                var childNodes = editionNode.Children.Where(x => x.DocumentTypeAlias == "PugPage");
                model.Pages = childNodes.Select(item => new Page(item));

                return View("~/App_Plugins/PugPigConnector/PugEdition.cshtml", model);
            }
            else // Return the publication feed
            {   
                var publicationNode = rm.Content.Parent;

                var model = new Publication(publicationNode, feedUrlAbsolute);

                var childNodes = publicationNode.Children.Where(x => x.DocumentTypeAlias == "PugEdition");
                model.Editions = childNodes.Select(item => new Edition(item, feedUrl));

                return View("~/App_Plugins/PugPigConnector/PugPublication.cshtml", model);
            }
        }
    }

}
