﻿using System;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Security;

namespace Umbraco.PugPigConnector.BusinessLogic.Helpers
{
    public static class UrlHelper
    {

        /// <summary>
        /// Produces a permanent URL for the pugPigUrl document property
        /// XML extension for the publication and edition nodes
        /// HTML extension for pages
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>URL as string</returns>
        internal static string CreatePugPigUrl(IContent entity)
        {
            switch (entity.ContentType.Alias)
            {
                case "PugPigPublication":
                    return string.Format("/{0}.xml", entity.Id);
                case "PugPigEdition":
                    return string.Format("/{0}/{1}.xml", entity.ParentId, entity.Id);
                case "PugPigPage":
                    return string.Format("/{0}/{1}/{2}.html", entity.Parent().ParentId, entity.ParentId, entity.Id);
            }

            return string.Empty;
        }

    }
}
