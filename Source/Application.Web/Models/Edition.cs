﻿using System;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.PugPigConnector.BusinessLogic.Extensions;
using Umbraco.Web;

namespace Umbraco.PugPigConnector.Web.Models
{
    public class Edition : PublishedContentModel
    {
        
        public Edition(IPublishedContent content)
            : base(content)
        { }

        public string Title
        {
            get { return this.GetPropertyValue<string>("title"); }
        }

        public string Updated
        {
            get { return this.UpdateDate.ToPugDate(); }
        }

        public string Published
        {
            get 
            {
                var date = this.GetPropertyValue<DateTime>("publishedDate") != DateTime.MinValue 
                                                                            ? this.GetPropertyValue<DateTime>("publishedDate") 
                                                                            : this.UpdateDate;
                return date.ToPugDate();
            }
        }

        public string Author
        {
            get { return this.GetPropertyValue<string>("authorName"); }
        }

        public string Summary
        {
            get { return this.GetPropertyValue<string>("summary"); }
        }

        public string CoverImage
        {
            get { return this.GetPropertyValue<string>("coverImage"); }
        }

        public string FeedUrl
        {
            get { return this.Url.RemoveTrailingSlash(); }
        }

        public IEnumerable<Page> Pages { get; set; }
    }
}