﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace Umbraco.PugPigConnector.BusinessLogic
{
    public class MyApplication : ApplicationEventHandler
	{
		protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
            UrlProviderResolver.Current.InsertTypeBefore<DefaultUrlProvider, FeedUrlProvider>();
		}
	}
    
    public class FeedUrlProvider : IUrlProvider
    {
        public string GetUrl(UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            var content = umbracoContext.ContentCache.GetById(id);
            if (content != null && (content.DocumentTypeAlias == "PugPublication" || content.DocumentTypeAlias == "PugEdition"))
            {
                // Add .xml to the Url
                return content.Url + ".xml";
            }

            return null;
        }

        public IEnumerable<string> GetOtherUrls(UmbracoContext umbracoContext, int id, Uri current)
        {
            return Enumerable.Empty<string>();
        }
    }
}