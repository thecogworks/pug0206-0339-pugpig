﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using HtmlAgilityPack;
using Umbraco.PugPigConnector.BusinessLogic.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Umbraco.PugPigConnector.BusinessLogic.Controllers
{
    public class PugPigPageController : RenderMvcController
    {
        public ActionResult PugPage(RenderModel rm)
        {
            if (!string.IsNullOrWhiteSpace(HttpContext.Request.QueryString["manifest"]) &&  HttpContext.Request.QueryString["manifest"] == "true")
            {
                // Return the cache manifest for this node
                // Get the node's defined cache manifest by from node properties and child data (parse out images and css etc)
                // 1. Get the cache manifest definition from the Publication root
                var publicationNode = rm.Content.AncestorOrSelf("PugPigPublication");

                // 2. Loop over the content of this pug page to get all the image Urls (using HtmlAgilityPack)
                var document = new HtmlWeb().Load(rm.Content.UrlAbsolute());
                var imageUrls = document.DocumentNode.Descendants("img")
                                        .Select(e => e.GetAttributeValue("src", null))
                                        .Where(s => !String.IsNullOrEmpty(s));

                var imageUrlStringBlock = new StringBuilder();
                foreach (var url in imageUrls)
                {
                    imageUrlStringBlock.AppendLine(url);
                }

                // 3. Plug these values into our viewmodel:
                var model = new CacheManifestViewModel
                {
                    Name = rm.Content.Name,
                    UpdateDate = rm.Content.UpdateDate.ToString(CultureInfo.InvariantCulture),
                    PublicationAssets = publicationNode.GetPropertyValue<string>("cacheManifest"),
                    PageAssets = imageUrlStringBlock.ToString()
                };

                return View("PugCacheManifest", model);
            }

            // It's not a cache manifest, just process the node as normal
            return CurrentTemplate(rm);
        }
    }

}
