window.addEventListener( 'DOMContentLoaded', function() {

  $.getJSON('../../scripts/data/data.json', function(data) {
    var template = $('.article-template').html();
    var html = Mustache.to_html(template, data);
    $('.pp-feature-01').html(html);
    $('.pp-feature-02').html(html);
    $('.pp-feature-03').html(html);
    $('.pp-contents-01').html(html);
    $('.pp-contents-02').html(html);

  });

});

$(document).ready(function(){

  //Click event to scroll to top
  $('.scrollToTop').click(function(){
    $('html, body').animate({scrollTop : 0},800);
    return false;
  });

});

function triggerSnapshotReady() {
  var iframe = document.createElement('iframe');
  iframe.setAttribute('src', 'pugpig://onPageReady');
  document.documentElement.appendChild(iframe);
  iframe.parentNode.removeChild(iframe);
  iframe = null;
}

function inApp() {
  return window.location.protocol.search(/http(s)?/) === -1;
}

document.addEventListener('DOMContentLoaded', function() {
  if (!inApp()) {
    // nothing
  } else {
    setTimeout(triggerSnapshotReady, 200);
  }
});