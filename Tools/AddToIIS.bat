@echo off
SET SITE=solution-template.local.tcwdigital.co.uk
SET APPCMD="%systemroot%\system32\inetsrv\appcmd.exe"


REM Ensure version number is given
IF .%1 == . (
	@echo off
	set /p ROOTPATH="Enter website root path without trailing slash (eg. e:\websites): " %=%
	@echo on
)
IF NOT .%1 == . (
	set ROOTPATH=%1
)


REM Create app pool
%APPCMD% add apppool /name:%SITE%

REM Create site
%APPCMD% add site /name:%SITE% /physicalPath:"%ROOTPATH%\%SITE%" /bindings:"http://%SITE%:80"

REM Attach app pool
%APPCMD% set app "%SITE%/" /applicationPool:%SITE% 