﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.PugPigConnector.Web.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Umbraco.PugPigConnector.Web.Controllers
{
    public class PugPublicationController : RenderMvcController
    {
        public ActionResult Index(RenderModel rm)
        {
            
            var model = new Publication(rm.Content);

            var childNodes = rm.Content.Children.Where(x => x.DocumentTypeAlias == "PugEdition");
            model.Editions = childNodes.Select(item => new Edition(item));

            return CurrentTemplate(model);
        }
    }

}
