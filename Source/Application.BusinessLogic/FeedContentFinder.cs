﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Web.Routing;

namespace Umbraco.PugPigConnector.BusinessLogic
{
    public class MyApplication : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentFinderResolver.Current.InsertTypeBefore<ContentFinderByNotFoundHandlers, FeedContentFinder>();
        }
    }
    
    public class FeedContentFinder : IContentFinder
    {
        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            if (contentRequest != null)
            {
                var path = contentRequest.Uri.GetAbsolutePathDecoded();

                if (path.EndsWith(".xml"))
                {
                    var parts = path.Split(new[] { '/',  }, System.StringSplitOptions.RemoveEmptyEntries);

                //    var urlPart = path.Substring()
                //    var node = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetByRoute(level3);

                //    if (node.DocumentTypeAlias == "Whatever")
                //        contentRequest.PublishedContent = node;
                }
            }

            return contentRequest.PublishedContent != null;
        }
    }

}
