﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Configuration;

namespace Application.Config
{
    public sealed class SampleSettings : ConfigurationSectionGroup
    {
        public static string SectionName = "SampleSettings";


        #region Singleton definition

        private static readonly SampleSettings _Settings;
        private SampleSettings() { }
        static SampleSettings()
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~\\Web.config");
            _Settings = config.GetSectionGroup(SectionName) as SampleSettings;
        }
        public static SampleSettings Instance
        {
            get { return _Settings; }
        }

        #endregion


        [ConfigurationProperty("SampleSettings")]
        public SampleConfig FormsyAppSettings
        {
            get
            {
                return (SampleConfig)base.Sections["FormsyAppSettings"];
            }
        }


    }
}
