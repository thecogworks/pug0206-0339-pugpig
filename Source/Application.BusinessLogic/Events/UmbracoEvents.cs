﻿using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.PugPigConnector.BusinessLogic.Helpers;
using Umbraco.Web;

namespace Umbraco.PugPigConnector.BusinessLogic.Events
{
    public class UmbracoEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication,
                                                    ApplicationContext applicationContext)
        {
            // Umbraco Events
            ContentService.Created += Content_New;
            ContentService.Saving += ContentService_Saving;
            ContentService.Published += ContentService_Published;
        }

        

        private void Content_New(IContentService sender, NewEventArgs<IContent> e)
        {
            // Set default PugPigStatus
            if (e.Entity.ContentType.Alias == "PugPigEdition" && e.Entity.HasProperty("status"))
            {
                // I need to find the Id of the 'Offline' prevalue option on the Status data type
                var dts = new DataTypeService();

                // Get an instance of the status editor
                var statusEditor = dts.GetAllDataTypeDefinitions().First(x => x.Name == "[PugPig] Status");

                // Now we can fetch the pre-values collection, and use linq to find the Id for the 'Reserved' option
                int preValueId = dts.GetPreValuesCollectionByDataTypeId(statusEditor.Id).PreValuesAsDictionary.Where(d => d.Value.Value == "Draft").Select(f => f.Value.Id).First();

                // Finally update the status with the new Id.
                e.Entity.SetValue("status", preValueId); 
            }
        }

        protected void ContentService_Saving(IContentService sender, SaveEventArgs<IContent> e)
        {
            foreach (var entity in e.SavedEntities.Where(entity => entity.ContentType.Alias == "PugPigPublication" || entity.ContentType.Alias == "PugPigEdition" || entity.ContentType.Alias == "PugPigPage"))
            {
                // Set node title. We can't do this in the Content_New event because we don't have the node name yet.
                if (entity.HasProperty("title") && string.IsNullOrWhiteSpace(entity.GetValue<string>("title")))
                {
                    entity.SetValue("title", entity.Name);
                }  
            }
        }

        private void ContentService_Published(Core.Publishing.IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            foreach (var entity in e.PublishedEntities
                                    .Where(entity => entity.ContentType.Alias == "PugPigPublication" || 
                                                     entity.ContentType.Alias == "PugPigEdition" || 
                                                     entity.ContentType.Alias == "PugPigPage"))
            {
                // Set Publication or Edition Feed Url. We can't do this in the Content_New or Content_Saving events because we don't have the node Id yet.
                if (entity.HasProperty("pugPigUrl"))
                {
                    var pugPigUrl = UrlHelper.CreatePugPigUrl(entity);
                    
                    // Only reset this property if the node path has changed
                    if (entity.GetValue<string>("pugPigUrl") == pugPigUrl) 
                        return;

                    entity.SetValue("pugPigUrl", pugPigUrl);

                    // Need to republish as we have changed the document
                    ApplicationContext.Current.Services.ContentService.SaveAndPublishWithStatus(entity, raiseEvents: false);
                }
            }
        }
        

    }
}
