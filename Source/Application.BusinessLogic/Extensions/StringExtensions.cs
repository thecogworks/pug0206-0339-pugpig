﻿using System;

namespace Umbraco.PugPigConnector.BusinessLogic.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveTrailingSlash(this string s)
        {
            var lastSlash = s.LastIndexOf('/');
            return (lastSlash > -1) ? s.Substring(0, lastSlash) : s;
        }

        public static string ToPugDate(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-ddThh:mm:sszzz");
        }

        public static string ToPermaLink(this int id)
        {
            return string.Format("/{0}.html", id);
        }

        public static string ToPermaLink(this int id, string fileExtension)
        {
            return string.Format("/{0}.{1}", id, fileExtension);
        }


    }
}
