﻿using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.PugPigConnector.BusinessLogic.Extensions;
using Umbraco.Web;

namespace Umbraco.PugPigConnector.BusinessLogic.Models
{
    public class Page : PublishedContentModel
    {
        public Page(IPublishedContent content) : base(content) { }

        public string Title
        {
            get { return this.GetPropertyValue<string>("title") ?? this.Name;  }
        }

        public string Updated
        {
            get { return this.UpdateDate.ToPugDate(); }
        }

        public string Summary
        {
            get { return this.GetPropertyValue<string>("summary"); }
        }

        public string CacheManifestUrl
        {
            // TODO: i'm pretty sure we just need to get the publication manifest?
            // ParentId.manifest
            get
            {
                //var url = string.Format("{0}/{1}/{2}.manifest", Content.Parent.Parent.Id, Content.Parent.Id, Id);
                var url = string.Format("{0}/{1}.manifest", Content.Parent.Id, Id);
                return url;
            }
        }

        public string PermaLink
        {
            get
            {
                //var url = string.Format("{0}/{1}/{2}.html", Content.Parent.Parent.Id, Content.Parent.Id, Id);
                var url = string.Format("{0}/{1}.html", Content.Parent.Id, Id);
                return url;

                //return this.Id.ToPermaLink();
            }
        }

        
    }
}